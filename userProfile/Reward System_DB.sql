
-- -----------------------------------------------------
-- Schema RewardDatabase
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `RewardDatabase` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `RewardDatabase` ;

-- -----------------------------------------------------
-- Table `RewardDatabase`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RewardDatabase`.`Users` (
  `User_ID` INT NOT NULL AUTO_INCREMENT,
  `User_Name` VARCHAR(45) NOT NULL,
  `User_Surname` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  `Restriction` char(1) NOT NULL,
  PRIMARY KEY (`User_ID`));



-- -----------------------------------------------------
-- Table `RewardDatabase`.`Bann`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RewardDatabase`.`Bann` (
  `Bann_ID` INT NOT NULL,
  `Bann_Name` VARCHAR(45) NOT NULL,
  `Adviser_ID` INT(11) NULL,
  PRIMARY KEY (`Bann_ID`)
  );



-- -----------------------------------------------------
-- Table `RewardDatabase`.`Students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RewardDatabase`.`Students` (
  `Student_ID` INT NOT NULL,
  `Bann_ID` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Student_ID`));









-- -----------------------------------------------------
-- Table `RewardDatabase`.`Rules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RewardDatabase`.`Rules` (
  `Rule_ID` INT NOT NULL,
  `Rule_Name` VARCHAR(45) NOT NULL,
  `Point` INT NOT NULL,
  `Duration` DATE NULL,
  PRIMARY KEY (`Rule_ID`));



-- -----------------------------------------------------
-- Table `RewardDatabase`.`Rewards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `RewardDatabase`.`Rewards` (
  `Student_ID` INT NOT NULL,
  `Instructure_D` INT NOT NULL,
  `Rule_ID` INT NOT NULL,
  `Time_Stamp` DATE NULL,
  PRIMARY KEY (`Student_ID`, `Instructure_D`, `Rule_ID`));
-- --------------------------------------------------------
-- Insert initial value
-- --------------------------------------------------------

INSERT INTO `users` VALUES (5488003,"Pana","Amphaisakul","administrator",'a');
INSERT INTO `users` VALUES (5488115,"Nutchanon","Phongoen","student",'s');
INSERT INTO `users` VALUES (1,"Bob","Swagger","instructor",'I');
INSERT INTO `students`(`Student_ID`, `Bann_ID`) VALUES (5488115,2);
INSERT INTO `bann`(`Bann_ID`, `Bann_Name`, `Adviser_ID`) VALUES (2,"TWO",1);

